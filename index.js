if ("serviceWorker" in navigator) {
  caches.open("edusys-static-v5").then(function(cache) {
    cache.delete("/");
    cache.delete("/home.php");
    cache.delete("/news.php");
    cache.delete("/todo.php");
    caches.open("edusys-static-v5").then(function(cache) {
      return cache.addAll([
        "/",
        "/home.php",
        "/news.php",
        "/todo.php"
      ]);
    });
    loadContent("home.php");
  });
  $(window).on("load", function() {
    navigator.serviceWorker.register("/sw.js").then(function(registration) {
      console.log("ServiceWorker registration successful with scope: ", registration.scope);
    }, function(err) {
      console.log("ServiceWorker registration failed: ", err);
    });
  });
}
$(document).ready(function() {
  $("#sideMenu").swipe({
    swipeRight: function(event, distance, duration, fingerCount, fingerData) {
      $(".mdl-layout__drawer").addClass("is-visible").attr("aria-hidden", "false");
      $(".mdl-layout__obfuscator").addClass("is-visible");
    },
    threshold: 0
  });
  $(".mdl-layout__drawer").swipe({
    swipeStatus: function(event, phase, direction, distance) {
      if (direction == "left") {
        $(".mdl-layout__drawer.is-visible").css({
          "-webkit-transform": "translateX(" + (-1.5 * distance) + "px)",
          "transform": "translateX(" + (-1.5 * distance) + "px)"
        });
        if ((phase == "cancel" || phase == "end") && distance > 100) {
          $(".mdl-layout__drawer").attr("aria-hidden", "true").removeClass("is-visible").attr("style", "");
          $(".mdl-layout__obfuscator").removeClass("is-visible");
        }
        if ((phase == "cancel" || phase == "end") && distance < 100) {
          $(".mdl-layout__drawer").attr("style", "");
        }
      }
    },
    allowPageScroll: "horizontal"
  });
});
$(window).on("load", function() {
  $("#loading").css("display", "none");
});
$(window).on("beforeunload", function() {
  $("#loading").css("display", "table");
});

function loadContent(a) {
  if ($(".mdl-layout__drawer").hasClass("is-visible")) {
    $(".mdl-layout__drawer").removeClass("is-visible");
    $(".mdl-layout__obfuscator").removeClass("is-visible");
  }
  $("#loading").css("display", "table");
  fetch(a).then(function(response) {
    response.text().then(function(text) {
      $(".cardContainer .mdl-grid").html(text);
      $("#loading").css("display", "none");
    });
  }).catch(function() {
    $("#loading").css("display", "none");
  });
}
loadContent("home.php");
