self.addEventListener("install", function(event) {
  caches.delete("edusys-static-v4").then(function() {
    event.waitUntil(
      caches.open("edusys-static-v5").then(function(cache) {
        var foreignRequestUrls = [
          "https://fonts.googleapis.com/icon?family=Material+Icons",
          "https://code.getmdl.io/1.3.0/material.red-blue.min.css",
          "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js",
          "https://code.getmdl.io/1.3.0/material.min.js",
        ];
        for (x in foreignRequestUrls) {
          const request = new Request(foreignRequestUrls[x], {
            mode: "no-cors"
          });
          fetch(request).then(response => cache.put(request, response));
        }
        return cache.addAll([
          "/",
          "/index.css",
          "/jquery.touchSwipe.min.js",
          "/index.js",
          "/home.php",
          "/news.php",
          "/todo.php"
        ]);
      })
    );
  });
});

self.addEventListener("fetch", function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      if (response) return response;
      return fetch(event.request);
    })
  );
});
