<?php
//include $_SERVER["DOCUMENT_ROOT"]."/databaseInfo.php";
include $_SERVER["DOCUMENT_ROOT"]."/functions.php";
$sql = "SELECT * FROM news WHERE expdate>='".date("Y-m-d")."' ORDER BY expdate";
$result = $conn->query($sql);
if($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    echo "            <div class=\"mdl-card mdl-cell mdl-cell--12-col mdl-shadow--2dp\">
                <div class=\"mdl-card__supporting-text\">
                  <h4 class=\"titleSub\">".$row["title"]."</h4>
                  <p>
                    ".$row["body"]."
                  </p>
                  <p class=\"postDate\">
                    Posted ".dateManip($row["pubdate"], 1).".
                  </p>
                </div>
              </div>
  ";
  }
}
else {
  echo "<h3>No news.</h3>";
}
?>
