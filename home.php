<?php
//include $_SERVER["DOCUMENT_ROOT"]."/databaseInfo.php";
include $_SERVER["DOCUMENT_ROOT"]."/functions.php";
$noToDo = 0;
function colContainerContent($a, $b, $c)
{
    $x = $y = $z = 0;
    if ($a > 0) {
        $x = 1;
    }
    if ($b > 0) {
        $y = 1;
    }
    if ($c > 0) {
        $z = 1;
    }
    $decVal = 4 * $x + 2 * $y + 1 * $z;
    if ($decVal == 0) {
        echo "                 <h4>Nothing to do</h4>
";
        return;
    }
    $multiplier = 2;
    $GLOBALS['noToDo'] = 1;
    if ($decVal == 1 || $decVal == 2 || $decVal == 4) {
        $multiplier = 12;
    } elseif ($decVal == 3 || $decVal == 5 || $decVal == 6) {
        $multiplier = 4;
    } else {
        $y = 2;
    }
    if ($a > 0) {
        echo "                 <div class=\"mdl-cell mdl-cell--4-col mdl-cell--".($x * $multiplier)."-col-tablet\">
                    <h1>$a</h1>
                    <h5>Assignment";
        if ($a > 1) {
            echo "s";
        }
        echo"</h5>
                  </div>
";
    }
    if ($b > 0) {
        echo "                  <div class=\"mdl-cell mdl-cell--4-col mdl-cell--".($y * $multiplier)."-col-tablet\">
                    <h1>$b</h1>
                    <h5>Homework";
        if ($b > 1) {
            echo "s";
        }
        echo"</h5>
                  </div>
";
    }
    if ($c > 0) {
        echo "                  <div class=\"mdl-cell mdl-cell--4-col mdl-cell--".($z * $multiplier)."-col-tablet\">
                    <h1>$c</h1>
                    <h5>Project";
        if ($c > 1) {
            echo "s";
        }
        echo"</h5>
                  </div>
";
    }
}
?>
  <div class="click mdl-card mdl-cell mdl-cell--12-col mdl-js-button mdl-js-ripple-effect mdl-shadow--2dp" onclick="loadContent('todo.php');">
    <div class="mdl-card__supporting-text">
      <h4>Today<br /><small><?php echo date("jS l")." (".timetableDay(date("Y-m-d"), 1).")";?></small></h4>
      <div class="colContainer mdl-grid">
<?php
$todaysDateTime = date("Y-m-d");
$sql = "SELECT * FROM todo WHERE type='assignment' AND duedate='$todaysDateTime'";
$result = $conn->query($sql);
$assgnCount = $result->num_rows;
$sql = "SELECT * FROM todo WHERE type='homework' AND duedate='$todaysDateTime'";
$result = $conn->query($sql);
$hwCount = $result->num_rows;
$sql = "SELECT * FROM todo WHERE type='project' AND duedate='$todaysDateTime'";
$result = $conn->query($sql);
$projCount = $result->num_rows;
colContainerContent($assgnCount, $hwCount, $projCount);
?>
      </div>
    </div>
<?php
if (4 * $assgnCount + 2 * $hwCount + 1 * $projCount > 0) {
echo "              <div class=\"mdl-card__actions mdl-card--border\">
        <a class=\"mdl-button mdl-button--colored\">
          Show List
        </a>
      </div>
";
}
?>
  </div>
  <div class="click mdl-card mdl-cell mdl-cell--12-col mdl-js-button mdl-js-ripple-effect mdl-shadow--2dp" onclick="loadContent('todo.php');">
    <div class="mdl-card__supporting-text">
      <h4>Tomorrow<br /><small><?php echo date("jS l", strtotime("+1 day", time()))." (".timetableDay(date("Y-m-d", strtotime("+1 day", time())), 1).")";?></small></h4>
      <div class="colContainer mdl-grid">
<?php
$todaysDateTime = date("Y-m-d", strtotime("+1 day", time()));
$sql = "SELECT * FROM todo WHERE type='assignment' AND duedate='$todaysDateTime'";
$result = $conn->query($sql);
$assgnCount = $result->num_rows;
$sql = "SELECT * FROM todo WHERE type='homework' AND duedate='$todaysDateTime'";
$result = $conn->query($sql);
$hwCount = $result->num_rows;
$sql = "SELECT * FROM todo WHERE type='project' AND duedate='$todaysDateTime'";
$result = $conn->query($sql);
$projCount = $result->num_rows;
colContainerContent($assgnCount, $hwCount, $projCount);
?>
      </div>
    </div>
<?php
if (4 * $assgnCount + 2 * $hwCount + 1 * $projCount > 0) {
echo "              <div class=\"mdl-card__actions mdl-card--border\">
      <a class=\"mdl-button mdl-button--colored\">
        Show List
      </a>
    </div>
";
}
?>
  </div>
<?php
if ($noToDo == 0) {
$subsql = "SELECT duedate FROM todo WHERE duedate > '".date("Y-m-d")."' LIMIT 1";
$result = $conn->query($subsql);
if ($result->num_rows > 0) {
$row = $result->fetch_assoc();
$nextDate = $row["duedate"];
echo "            <div class=\"click mdl-card mdl-cell mdl-cell--12-col mdl-js-button mdl-js-ripple-effect mdl-shadow--2dp\" onclick=\"loadContent('todo.php');\">
    <div class=\"mdl-card__supporting-text\">
      <h4>".date("jS l", strtotime($nextDate))."<br /><small>".timetableDay(date("Y-m-d", strtotime($nextDate)), 1)."</small></h4>
      <div class=\"colContainer mdl-grid\">
";
$sql = "SELECT * FROM todo WHERE type='assignment' AND duedate=($subsql)";
$result = $conn->query($sql);
$assgnCount = $result->num_rows;
$sql = "SELECT * FROM todo WHERE type='homework' AND duedate=($subsql)";
$result = $conn->query($sql);
$hwCount = $result->num_rows;
$sql = "SELECT * FROM todo WHERE type='project' AND duedate=($subsql)";
$result = $conn->query($sql);
$projCount = $result->num_rows;
colContainerContent($assgnCount, $hwCount, $projCount);
echo "                </div>
    </div>
";
}
}
?>
  </div>
