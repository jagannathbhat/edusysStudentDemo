<?php
//include $_SERVER["DOCUMENT_ROOT"]."/databaseInfo.php";
include $_SERVER["DOCUMENT_ROOT"]."/functions.php";
$sql = "SELECT * FROM todo WHERE duedate>='".date("Y-m-d")."' ORDER BY duedate";
$result = $conn->query($sql);
if($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    echo "            <div class=\"mdl-card mdl-cell mdl-cell--12-col mdl-shadow--2dp\">
                <div class=\"mdl-card__supporting-text\">
                  <h4 class=\"titleSub\">".$row["title"]."</h4>
                  <h6 class=\"subTitle\">".dateManip($row["duedate"])." (".timetableDay($row["duedate"], 1).")</h6>
                  <p>
                    ".$row["body"]."
                  </p>
                  <p class=\"postDate\">
                    Posted on ".dateManip($row["pubdate"]).".
                  </p>
                </div>
              </div>
  ";
  }
}
else {
  echo "<h3>Nothing to do.</h3>";
}
?>
