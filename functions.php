<?php
function dateManip($givenDate, $flag = 0)
{
    $givenDate = strtotime($givenDate);
    if (date("j n Y") == date("j n Y", $givenDate)) {
        if ($flag) {
            return "today";
        } else {
            return "Today";
        }
    } elseif (date("j n Y", strtotime("-1 day", time())) == date("j n Y", $givenDate)) {
        if ($flag) {
            return "yesterday";
        } else {
            return "Yesterday";
        }
    } elseif (date("j n Y", strtotime("+1 day", time())) == date("j n Y", $givenDate)) {
        if ($flag) {
            return "tomorrow";
        } else {
            return "Tomorrow";
        }
    } elseif (date("n Y") == date("n Y", $givenDate)) {
        if ($flag) {
            return "on ".date("jS l", $givenDate);
        } else {
            return date("jS l", $givenDate);
        }
    } elseif (date("Y") == date("Y", $givenDate)) {
        if ($flag) {
            return "on ".date("jS M (D)", $givenDate);
        } else {
            return date("jS M (D)", $givenDate);
        }
    } else {
        if ($flag) {
            return "on ".date("jS M y (D)", $givenDate);
        } else {
            return date("jS M y (D)", $givenDate);
        }
    }
}
function isHol($d)
{
    global $conn;
    $sql = "SELECT * FROM holidays WHERE holdate='$d'";
    $result = $conn->query($sql);
    if($result->num_rows > 0) {
      return 1;
    }
    else {
      $sql = "SELECT * FROM holidays WHERE holdate<'$d' ORDER BY holdate DESC LIMIT 1";
      $result = $conn->query($sql);
      if($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $diff = strtotime($d) - strtotime($row["holdate"]);
        $daysDiff = $diff / 60 / 60 / 24;
        if($daysDiff >= $row["noofdays"]) {
          return 0;
        }
        else {
          return 1;
        }
      }
      else {
        return 0;
      }
    }
}
function noOfHols($d)
{
    global $conn;
    $sql = "SELECT SUM(noofdays) AS tot FROM holidays WHERE holdate<='$d'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    if ($row["tot"] != NULL) {
        return $row["tot"];
    }
    else {
        return 0;
    }
}
function timetableDay($thatDate, $flag = 0)
{
    $genDate = "2018-01-01";
    if (date("l", strtotime($thatDate)) == "Saturday" || date("l", strtotime($thatDate)) == "Sunday" || isHol($thatDate)) {
        if ($flag == 1) {
            return "Holiday";
        }
        return 0;
    }
    else {
        $diff = strtotime($thatDate) - strtotime($genDate);
        $daysDiff = $diff / 60 / 60 / 24;
        $noOfSats = intdiv($daysDiff + 2, 7);
        $workingDays = $daysDiff - 2 - 2 * $noOfSats - noOfHols($thatDate);
        $day = $workingDays % 5 + 1;
        if ($flag == 1) {
            return "Day $day";
        }
        return $day;
    }
}
?>
