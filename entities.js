var noToDo = 0;
var days = [
  {
    title: "Today",
    date: "26th Friday (Holiday)",
    assignments: 0,
    homeworks: 0,
    projects: 0
  }, {
    title: "Tomorrow",
    date: "27th Saturday (Holiday)",
    assignments: 0,
    homeworks: 0,
    projects: 0
  }, {
    title: "29th Monday",
    date: "Day 2",
    assignments: 2,
    homeworks: 0,
    projects: 0
  }
];
var todo = [
  {
    title: "OODP Tutorial",
    date: "29th Monday (Day 2)",
    body: "Write the answers to the tutorial questions given in Moodle and in class today, in the tutorial book.",
    posted: "Posted yesterday."
  }, {
    title: "Maths Tutorial",
    date: "29th Monday (Day 2)",
    body: "Bring the maths tutorial book to class on the 29th (Monday).",
    posted: "Posted yesterday."
  }
];
function taskCount(a, b, c) {
  var x = 0, y = 0, z = 0;
  if (a > 0) x = 1;
  if (b > 0) y = 1;
  if (c > 0) z = 1;
  var decVal = 4 * x + 2 * y + 1 * z;
  var content = "";
  if (decVal == 0) {
      return "<h4>Nothing to do</h4>";
  }
  var returnContent = "";
  var multiplier = 2;
  noToDo = 1;
  if (decVal == 1 || decVal == 2 || decVal == 4) {
      multiplier = 12;
  }
  else if (decVal == 3 || decVal == 5 || decVal == 6) {
      multiplier = 4;
  }
  else {
      y = 2;
  }
  if (a > 0) {
      returnContent +=  "<div class=\"mdl-cell mdl-cell--4-col mdl-cell--" + (x * multiplier) + "-col-tablet\"><h1>" + a + "</h1><h5>Assignment";
      if (a > 1) {
          returnContent += "s";
      }
      returnContent +="</h5></div>";
  }
  if (b > 0) {
      returnContent +=  "<div class=\"mdl-cell mdl-cell--4-col mdl-cell--" + (y * multiplier) + "-col-tablet\"><h1>" + b + "</h1><h5>Homework";
      if (a > 1) {
          returnContent += "s";
      }
      returnContent +="</h5></div>";
  }
  if (c > 0) {
      returnContent +=  "<div class=\"mdl-cell mdl-cell--4-col mdl-cell--" + (z * multiplier) + "-col-tablet\"><h1>" + b + "</h1><h5>Project";
      if (a > 1) {
          returnContent += "s";
      }
      returnContent +="</h5></div>";
  }
  return returnContent;
}
var pourContent = {
  homeContent: function() {
    if ($(".mdl-layout__drawer").hasClass("is-visible")) {
      $(".mdl-layout__drawer").removeClass("is-visible");
      $(".mdl-layout__obfuscator").removeClass("is-visible");
    }
    document.querySelector(".cardContainer .mdl-grid").innerHTML = "";
    for(x in days) {
      var card = document.createElement("div");
      card.classList = "click mdl-card mdl-cell mdl-cell--12-col mdl-js-button mdl-js-ripple-effect mdl-shadow--2dp";
      var supportingText = document.createElement("div");
      supportingText.classList = "mdl-card__supporting-text";
      var title = document.createElement("h4");
      title.innerHTML = days[x].title + "<br /><small>" + days[x].date + "</small>";
      var colContainer = document.createElement("div");
      colContainer.classList = "colContainer mdl-grid";
      colContainer.innerHTML = taskCount(days[x].assignments, days[x].homeworks, days[x].projects);
      supportingText.appendChild(title);
      supportingText.appendChild(colContainer);
      card.appendChild(supportingText);
      document.querySelector(".cardContainer .mdl-grid").appendChild(card);
    }
  },
  todoContent: function() {
    if ($(".mdl-layout__drawer").hasClass("is-visible")) {
      $(".mdl-layout__drawer").removeClass("is-visible");
      $(".mdl-layout__obfuscator").removeClass("is-visible");
    }
    document.querySelector(".cardContainer .mdl-grid").innerHTML = "";
    for(x in todo) {
      var card = document.createElement("div");
      card.classList = "click mdl-card mdl-cell mdl-cell--12-col mdl-js-button mdl-js-ripple-effect mdl-shadow--2dp";
      var supportingText = document.createElement("div");
      supportingText.classList = "mdl-card__supporting-text";
      var title = document.createElement("h4");
      title.innerHTML = todo[x].title + "<br /><small>" + todo[x].date + "</small>";
      var cardBody = document.createElement("p");
      cardBody.innerHTML = todo[x].body;
      var postedText = document.createElement("p");
      postedText.innerHTML = todo[x].posted;
      supportingText.appendChild(title);
      supportingText.appendChild(cardBody);
      supportingText.appendChild(postedText);
      card.appendChild(supportingText);
      document.querySelector(".cardContainer .mdl-grid").appendChild(card);
    }
  }
}
pourContent.homeContent();
